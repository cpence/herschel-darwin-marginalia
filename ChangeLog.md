
**Version History**

*   v1.2 (August 6, 2020): Fix some accidental Markdown list formatting that was
    causing trouble when rendering the marginalia file.
*   v1.1 (September 2, 2017): Add two details about the publisher's inscription.
*   v1.0 (August 28, 2015): Content all present, _Origin_ quotes checked against
    <http://darwin-online.org.uk>
