
Sir John F.W. Herschel's Marginalia on Darwin's _Origin of Species_
===================================================================

*Originally copied from volume by:* Prof. Stephen R. Case, Olivet Nazarene
University  
*Edited and published by:* Prof. Charles H. Pence, Université catholique de
Louvain

This book may be found in the Herschel Family Archive at the Harry Ransom
Center, University of Texas at Austin. Steve Case was kind enough to copy them
out and provide them to me, and I have checked all quotations against the
_Origin_ and recompiled them here for the aid of other scholars. Assistance with
the editorial material has been provided by Andre Ariew.

Editorial notes: Passages of Darwin which Herschel has scored in the margins are
copied out verbatim. _Italic text_ indicates Herschel's underlining, _*bold
italics*_ indicates double underlining. In several places, Herschel marked the
margin with a 'C' (or a curl looking like a C), apparently to indicate
contradictions.

----------

_On the Origin of Species by Means of Natural Selection, or the Preservation of
Favoured Races in the Struggle for Life_ (London, John Murray, Albemarle Street,
1859 [1st. ed])

**Front Matter**

*   first page: inscribed, "From the Author"

    [**Editor:** This inscription was written by someone at the publisher's
    office, as Darwin instructed the publisher to send a copy directly to
    Herschel. See [a letter from Darwin to Herschel, November 11,
    1859.](http://www.darwinproject.ac.uk/DCP-LETT-2517)]

*   seventh page: [Herschel Library Collingwood stamp]

**Chapter 1**

*   p. 30: [some marginal scoring of text related to breeding]

*   p. 30: The key is man's power of accumulative selection: _nature_ gives
    successive variations; _man adds_ them up in certain directions useful to
    him. In this sense _*he* may be said to *make*_ for himself useful breeds.

*   pp. 38-9: He can never act by selection, excepting on variations [39] which
    are first given to him in some slight degree by nature.

*   p. 39: But to use such an expression as trying to make a fantail, is, I have
    no doubt, in most cases, utterly incorrect.

*   p. 43: I do not believe that variability is an inherent and necessary
    contingency, under all circumstances, with all organic beings, as some
    authors have thought. [marked with C]

*   p. 43: Over all these causes of Change I am convinced that the accumulative
    action of Selection, whether applied methodically and more quickly, or
    unconsciously and more slowly, but more efficiently, is by far the
    predominant power. [marked with C]

**Chapter 2**

*   p. 57: incipient species greater than the average are now manufacturing,
    many of the species already manufactured still to a certain extent resemble
    varieties

*   p. 59: The larger genera thus tend to become larger

*   p. 59: the larger genera also tend to break up into smaller genera

    [**Editor:** Herschel has connected these two phrases on page 59 with '!!',
    obviously not pleased at the apparent contradiction.]

**Chapter 3**

*   p. 60: all those _exquisite_ adaptations

*   p. 60: We see these _beautiful_ co-adaptations

*   p. 61: in short, we see _beautiful_ adaptations everywhere

*   p. 61: _All_ these results, as we shall more fully see in the next chapter,
    follow _inevitably_ from the struggle for life

*   p. 61: But Natural Selection, as we shall hereafter see, is a _power_
    incessantly ready for action

**Chapter 4**

*   p. 82: as man can certainly produce great results by adding up in any given
    direction mere individual differences, _so_ could _*Nature,* but far more
    easily,_ from having incomparably longer _time at her disposal._ [marked
    with C]

*   p. 83: _nature cares nothing_ for appearances, except in so far as they may
    be useful to any being. [marked with C]

*   p. 83: _She can act_ on every internal organ [marked with C]

*   p. 83: Man selects only for his own good; Nature only for that of the being
    which _she tends._ [marked with C]

*   p. 84: _*should plainly bear the stamp of far higher workmanship?*_ [marked
    with C]

*   p. 87: Now, _if nature *had to make* the beak of a full-grown pigeon very
    short for the bird's own advantage,_ [marked with C]

*   pp. 93-4: No naturalist doubts the advantage of what has been called the
    "physiological division of labour;" hence we may believe that it would be
    advantageous to a plant to produce stamens alone in one flower or on one
    whole plant, and pistils alone in [94] another flower or on another
    plant. In plants under culture and placed under new conditions of life,
    sometimes the male organs and sometimes the female organs become more or
    less impotent; now if we suppose this to occur in ever so slight a degree
    under nature, then as pollen is already carried regularly from flower to
    flower, and as a more complete separation of the sexes of our plant would be
    advantageous on the principle of the division of labour, individuals with
    this tendency more and more increased, would be continually favoured or
    selected, until at last a complete separation of the sexes would be
    effected. [entire paragraph marked with large !]

*   p. 94: Let us now turn to the nectar-feeding insects in our imaginary case:
    we may suppose the plant of which we have been slowly increasing the nectar
    by continued selection, to be a common plant; and that certain insects
    depended in main part on its nectar for food. I could give many facts,
    showing how anxious bees are to save time; for instance, their habit of
    cutting holes and sucking the nectar at the bases of certain flowers, which
    they can, with a very little more trouble, enter by the mouth. Bearing such
    facts in mind, I can see no reason to doubt that an accidental deviation in
    the size and form of the body, or in the curvature and length of the
    proboscis, &c., far too slight to be appreciated by us, might profit a bee
    or other insect, so that an individual so characterised would be able to
    obtain its food more quickly, and so have a better chance of living and
    leaving descendants. Its descendants would probably inherit a tendency to a
    similar slight deviation of structure. The tubes of the corollas of the
    common red and incarnate clovers (Trifolium pratense and incarnatum) do not
    on a hasty glance appear to differ in length; yet the hive-bee can easily
    suck the nectar out of the incarnate clover, but not out of the common
    red... [marked in margin: "? ! Working bees have no descendants"]

*   p. 95: ...clover, which is visited by humble-bees alone; so that whole
    fields of the red clover offer in vain an abundant supply of precious nectar
    to the hive-bee. Thus it might be a great advantage to the hive-bee to have
    a slightly longer or differently constructed proboscis. On the other hand, I
    have found by experiment that the fertility of clover greatly depends on
    bees visiting and moving parts of the corolla, so as to push the pollen on
    to the stigmatic surface. Hence, again, if humble-bees were to become rare
    in any country, it might be a great advantage to the red clover to have a
    shorter or more deeply divided tube to its corolla, so that the hive-bee
    could visit its flowers. Thus I can understand how a flower and a bee might
    slowly become, either simultaneously or one after the other, modified and
    adapted in the most perfect manner to each other, by the continued
    preservation of individuals presenting mutual and slightly favourable
    deviations of structure. [marked in margin: "Why does he not cut a hole?",
    referring to previous passage on p. 94]

*   p. 95: Natural selection can act only by the preservation and accumulation
    of infinitesimally small inherited modifications, each profitable to the
    preserved being

*   p. 99: If several varieties of the cabbage, radish, onion, and of some other
    plants, be allowed to seed near each other, a large majority, as I have
    found, of the seedlings thus raised will turn out mongrels: for instance, I
    raised 233 seedling cabbages from some plants of different varieties growing
    near each other, and of these only 78 were true to their kind, and some even
    of these were not perfectly true. Yet the pistil of each cabbage-flower is
    surrounded not only by its own six stamens, but by those of the many other
    flowers on the same plant. How, then, comes it that such a vast number of
    the seedlings are mongrelized? I suspect that it must arise from the pollen
    of a distinct variety having a prepotent effect over a flower's own pollen;
    and that this is part of the general law of good being derived from the
    intercrossing of distinct individuals of the same species. When distinct
    species are crossed the case is directly the reverse, for a plant's own
    pollen is always prepotent over foreign pollen; but to this subject we shall
    return in a future chapter.

*   p. 100: flowers on the same tree can be considered as distinct individuals
    only in a limited sense. I believe this objection to be valid, but that
    nature has largely provided against it by giving to trees a strong tendency
    to bear flowers with separated sexes.

*   p. 108: Nothing can be _effected_ unless favourable variations _occur_
    [marked with C]

*   p. 109: Slow though the process of selection may be, if feeble man can do
    much by his powers of artificial selection, I can see no limit to the amount
    of change, to the beauty and infinite complexity of the coadaptations
    between all organic beings, one with another and with their physical
    conditions of life, which may be effected in the long course of time by
    nature's power of selection. [marked with C]

**Chapter 5**

*   p. 131: I HAVE hitherto sometimes spoken as if the variations—so common and
    multiform in organic beings under domestication, and in a lesser degree in
    those in a state of nature—had been due to chance. [marked with X]

*   p. 135: [regarding the ostrich] _its legs were used more, and its wings
    less,_ until they became incapable of flight. [marked in margin: "_*Why*_
    not vice versa like the albatross Shore feet are defective for surviving,
    whereas the [illeg.] headed duck has exact opposite characteristics tho'
    descended, on this theory from the same stock as the albatross", also marked
    with C]

*   p. 146: Hence we see that modifications of structure, viewed by systematists
    as of high value, may be wholly due to unknown laws of correlated growth,
    and without being, as far as we can see, of the slightest service to the
    species.

*   p. 149: beings low in the scale of nature are more variable than those which
    are higher. [marked in margin: "X X See page 313"]

*   p. 161: in each successive generation there has been a tendency to reproduce
    the character in question, which at last, under unknown favourable
    conditions, gains an ascendancy. [marked in margin: "Wherein lies the
    distinction?"]

*   p. 170: Whatever the cause may be of each slight difference in the offspring
    from their parents -- _and a cause for each must exist_ -- it is the steady
    accumulation, through natural selection, of such differences, when
    beneficial to the individual, that _*gives rise*_ to all the more important
    modifications of structure, by which the innumerable beings on the face of
    this earth are enabled to struggle with each other, and the best adapted to
    survive. [marked with C]

*   p. 170: [Herschel's annotation below this paragraph:] D. recognizes an
    unknown cause of slight individual differences -- but claims for "natural
    selection" the character of a "sufficient theory" in regard to the results
    of those differences.

**Chapter 6**

*   p. 178: polity of the country can be better filled by some modification of
    some one or more of its inhabitants.

*   pp. 185-6: He who believes in separate and innumerable acts of creation will
    say, that in these cases it has pleased the [186] Creator to cause a being
    of one type to take the place of one of another type; but this seems to me
    only restating the fact in dignified language. He who believes in the
    struggle for existence and in the principle of natural selection, will
    acknowledge that every organic being is constantly endeavouring to increase
    in numbers; and that if any one being vary ever so little, either in habits
    or structure, and thus gain an advantage over some other inhabitant of the
    country, it will seize on the place of that inhabitant, however different it
    may be from its own place. [marked with backward C]

*   p. 194: Why, on the theory of Creation, should this be so? Why should all
    the parts and organs of many independent beings, each supposed to have been
    separately created for its proper place in nature, be so invariably linked
    together by graduated steps? Why should not Nature have taken a leap from
    structure to structure? On the theory of natural selection, we can clearly
    understand why she should not; for natural selection can act only by taking
    advantage of slight successive variations; she can never take a leap, but
    must advance by the shortest and slowest steps. [marked with C]

**Chapter 7**

*   p. 207: I must premise, that I have nothing to do with the origin of the
    primary mental powers, any more than I have with that of life
    itself. [marked with C]

*   p. 219: [regarding slave-making ants] _*carry their masters*_ in their
    jaws. [marked with NB]

*   p. 221: _as_ Huber has described, _their slaves in their jaws._ [marked in
    margin: "NB F. Sanguinea"]

*   p. 243-4: Finally, it may not be a logical deduction, but to my imagination
    it is far more satisfactory to look at such instincts as the young [244]
    cuckoo ejecting its foster-brothers, -- ants making slaves, -- the larvae of
    ichneumonidae feeding within the live bodies of caterpillars, -- not as
    specially endowed or created instincts, but as small consequences of one
    general law, leading to the advancement of all organic beings, namely,
    multiply, vary, let the strongest live and the weakest die. [marked with C]

**Chapter 8**

[no notes]

**Chapter 9**

[no notes]

**Chapter 10**

*   p. 313: There is some reason to believe that organisms, considered high in
    the scale of nature, change more quickly than those that are low [marked in
    margin: "see page 149."]

**Chapter 11**

*   p. 352: He who rejects it, rejects the vera causa of ordinary generation
    with subsequent migration, and calls in the agency of a miracle. [marked
    with X]

**Chapter 12**

*   p. 388: Nature, like a careful gardener, thus takes her seeds from a bed of
    a particular nature, and drops them in another equally well fitted for
    them. [marked with large X]

**Chapter 13**

[no notes]

**Chapter 14**

[no notes]

**Back Matter**

[**Editor:** The back blank page contains the following notes:]

p. 131 Re chance  
p. 182 The eye  
p. 351 Law of ... development  
30\.  
57 manufactions[?]  
60  
90, L division of ....  
99\. 100  
  
43\. Variability not necessary condition of organized matter  
100\. favorable variations must "occur", if anything is to be "effected"  
3\. admination[?] ?of what on whome?  
388\. Nature as a .... _agent_ 82-83, 84-  *87*  
30\. Man using Nature's blind agency, he being intelligent but subject to remark
at p. 38 & 43 last lines  
109\. Nature using her "power"  
126 et aute "the Polity of Nature"  
170  
186 "The Creation" a "dignified" work for "natural selection"  
194 "Why" old diffc[?] creatures resemble each other is *theory*[?]?

[**Editor:** An inserted loose page contains the following notes:]

~~Mere change (we may call it) *might*~~  
F  
131\. Variations are due to "chance" explained to be "an unknown cause in each
particular case"  
207; Has nothing to do with the origin of "life" or "mental powers" eg
instinct  
244 not even "instincts" he thinks are "accumulated" & not "endowed" at least
not "specially" If not "logical" it is "more satisfactory to his imagination" to
think it so
